from flask import Flask, request, jsonify
import socket
import os
from utils import  NUMBER, SQUARE, METHOD_GET, METHOD_POST, PORT

app = Flask(__name__)

@app.route('/', methods=[METHOD_GET])
def index():
    return jsonify({'message': f'Hello World from {socket.gethostname()}'})

@app.route('/square', methods=[METHOD_POST])
def login():
    rq = request.get_json(silent=True)

    try:
        if rq[NUMBER] is None:
            print("please fill the attribute number")
            return jsonify({'message': "An error happened. We couldn't read the variable number. Please fill the attribute number"}) 
        if rq[SQUARE] is None:
            print("please fill the attribute square")
            return jsonify({'message': "An error happened. We couldn't read the variable square. Please fill the attribute square"})

        return jsonify({'message': f'Result of X square Y : {int(rq[NUMBER]) ** int(rq[SQUARE])}'})
    except:
        print("Something went wrong")
        return jsonify({'message': "Something went wrong"})


if __name__ == '__main__':
    app.run(
        debug=True, 
        host="0.0.0.0",
        port= os.getenv(PORT) if os.getenv(PORT) else 5000
        )