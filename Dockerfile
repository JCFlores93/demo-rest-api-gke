FROM python:3-alpine

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

COPY ./src/ /app

EXPOSE 5000

ENTRYPOINT [ "python" ]

CMD [ "main.py" ]