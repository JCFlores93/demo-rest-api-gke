build:
	- docker build -t flask-demo:latest .

run:
	- docker run -d -p 5000:5000 flask-demo

tag:
	- docker tag flask-demo gcr.io/terraform-demo-282919/flask-demo:latest

push: 
	- docker push gcr.io/terraform-demo-282919/flask-demo:latest